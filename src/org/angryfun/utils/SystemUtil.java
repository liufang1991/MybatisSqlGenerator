/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.angryfun.utils;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

/**
 *
 * @author liufang
 */
public class SystemUtil {

    /**
     * 向剪贴板中添加内容
     *
     * @param content
     */
    public static void setClipbordContents(String contents) {
        StringSelection stringSelection = new StringSelection(contents);
        // 系统剪贴板
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
    }
}
