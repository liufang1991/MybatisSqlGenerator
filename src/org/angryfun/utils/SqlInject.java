/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.angryfun.utils;

/**
 *
 * @author liufang
 */
public class SqlInject {
    public static String fillSql(String database,String sqlNoParamString,String paramString){
        String sqlNew="";
        String[] sqlArray=sqlNoParamString.trim().split("[?]");//如果sql语句最后一个为？数组和没有？是一样的结果
        String[] paramArray=paramString.split(", ");//加上空格是为了过滤掉有类似1,2(String)这样的字段值
        String paramType="integer";//integer string double timestamp
        int count=sqlArray.length;
        for(int i=0;i<count;i++){
        	sqlNew+=sqlArray[i];
        	if(i==(count-1)&&!sqlNoParamString.trim().endsWith("?")){//如果最后一个？不在末尾，则已经没有参数了
        	  break;
        	}
        	if(paramArray[i].trim().equals("null")){//null操作
        		sqlNew=sqlNew+paramArray[i];
        		continue;
        	}
        	paramType=paramArray[i].substring(paramArray[i].indexOf('(')+1, paramArray[i].indexOf(')')).toLowerCase();
        	if(paramType.equals("string")){
        		sqlNew=sqlNew+"'"+paramArray[i].substring(0,paramArray[i].indexOf('(')).trim()+"'";
        	}else if("oracle".equals(database)&&"timestamp".equalsIgnoreCase(paramType)){
        		sqlNew=sqlNew+"to_timestamp('"+paramArray[i].substring(0,paramArray[i].indexOf('(')).replace(".0","")+"','yyyy-mm-dd hh24:mi:ss')";
        	}else{
        		sqlNew=sqlNew+paramArray[i].substring(0,paramArray[i].indexOf('('));
        	}
        }
        return sqlNew;
    }
}
