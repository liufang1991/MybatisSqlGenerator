用来填充mybatis的sql语句，原理很简单
----
博文地址：http://blog.csdn.net/liufang1991/article/details/51243642
===
使用：进入dist文件夹，找到MybatisSqlGenerator.jar双击


或者运行命令java -jar MybatisSqlGenerator.jar

1. mysql数据库填充没有问题，现在开始完善对oracle数据库的支持

2. 如果启动文本框聚焦自动粘贴，在文本框聚焦的时候会自动把sql语句填充到文本框中，如果不习惯可以不启动该功能

3. 如果勾选了第二个文本框，则可以把sql语句和参数两行文本一起复制到第一个文本框中，因为解析是根据Preparing开始解析的，要求第一行的sql语句必须要包含Preparing:

4. 点击输出以后会自动把输出的结果保存到剪切板中